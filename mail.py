#!/usr/bin/env python

"""Verifies that sending emails to all configured accounts works."""

from __future__ import print_function

import sys
import datetime
import socket
import smtplib
from email.mime.text import MIMEText
import imaplib
import uuid
import ConfigParser
import time

import settings
from shared import print_stderr, ABORT_ITERATION, execute


def main():
    description = 'Verifies that sending emails to all configured accounts works.'
    file_argument_help = 'a list of configuration files with email accounts to test'
    test_steps = [generate_unique_subject, send_mail, check_for_mail]
    return execute(description, file_argument_help, read_email_accounts_to_test, test_steps)


def read_email_accounts_to_test(file):
    """read the email account to test from an ini-file.

    The structure must be as follows:
    [NAME_OF_THE_PROVIDER]
    address=email@name_of_the_provider
    hostname=hostname.name_of_the_provider
    protocol=IMAP
    username=username@name_of_the_provider
    password=****
    There can be multiple blocks in the same file for multiple providers.
    """
    config = ConfigParser.RawConfigParser()
    config.readfp(file)
    for section in config.sections():
        yield dict(config.items(section), name=section)


def generate_unique_subject(**_):
    """returns a universally unique identifier"""
    return True, {'subject': str(uuid.uuid4())}


def send_mail(address, subject, **_):
    """sends an email using SMTP"""
    message = MIMEText("""Ignore this message. It is a test message from the Chaosnet email testing software.
                          Sent from %s at %s.""" % (socket.gethostname(), datetime.datetime.now()))
    message['From'] = settings.SENDER_ADDRESS
    message['To'] = address
    message['Subject'] = subject
    server = smtplib.SMTP(settings.SMTP_SERVER)
    # server.login('', '') # For testing from remote machines
    try:
        server.sendmail(settings.SENDER_ADDRESS, [address], message.as_string())
    except smtplib.SMTPRecipientsRefused as exception:
        for recipient, error in exception.recipients.iteritems():
            print_stderr("Error sending email to %s. %s replied: %s" % (recipient, settings.SMTP_SERVER, error))
        return ABORT_ITERATION, None
    server.quit()
    return True, None


def check_for_mail_imap(hostname, username, password, subject):
    """test if the mail was received using IMAP"""
    message_found = False

    try:
        server = imaplib.IMAP4_SSL(hostname)
        server.login(username, password)
        server.select()  # open the INBOX
        _, data = server.search(None, 'ALL')
        for message_number in data[0].split():
            _, envelope = server.fetch(message_number, '(ENVELOPE)')
            if subject in envelope[0]:
                message_found = True
            server.store(message_number, '+FLAGS', '\\Deleted')  # delete the message
        server.expunge()
        server.close()
        server.logout()
    except imaplib.IMAP4.error as error:
        print_stderr("Error on %s: %s" % (hostname, error))
    return message_found


# How long (in seconds) to wait between subsequent tries. No need to wait after the last call, thus has to be 0.
SLEEP_TIMES = [5, 60, 0]
# Supported protocols to check for mail.
MAIL_CHECKING_FUNCTIONS_BY_PROTOCOL = {'IMAP': check_for_mail_imap}


def check_for_mail(protocol, name, hostname, username, password, subject, verbose, **_):
    """test if the mail was received. Tries multiple times (see SLEEP_TIMES) and waits in between."""
    try:
        check_for_mail_function = MAIL_CHECKING_FUNCTIONS_BY_PROTOCOL[protocol]
    except KeyError:
        print_stderr('Unsupported protocol: %s' % protocol)
        return ABORT_ITERATION, None

    for sleep_time in SLEEP_TIMES:
        if check_for_mail_function(hostname, username, password, subject):
            if verbose:
                print("Message on %s received." % name)
            return True, None
        time.sleep(sleep_time)
    print_stderr("Message on %s not received." % name)
    return False, None


if __name__ == '__main__':
    sys.exit(main())
