from __future__ import print_function

import unittest
from io import StringIO  # Python2: from io import BytesIO as StringIO

from dns import name, rdataclass, rdatatype, rdataset, message, exception, node, rrset
from dns.rdtypes.ANY import NS, SOA, TXT
from mock import patch, call, ANY

import dns_bind
import settings
from shared_test import patch_stderr, patch_stdout

TTL = 86400
DOMAIN = name.from_text('example.org')
RESPONSIBLE = name.from_text('hostmaster', DOMAIN)
DNS0 = name.from_text('dns0', DOMAIN)
DNS1 = name.from_text('dns1', DOMAIN)
SERIAL = 1234
REFRESH = 7200
RETRY = 1200
EXPIRE = 1296000
MINIMUM = 3600

ZONE_FILE = """@ IN SOA {master} {responsible} {serial} {refresh} {retry} {expire} {minimum}
@ IN NS {ns0}
@ IN NS {ns1}""".format(master=DNS0, responsible=RESPONSIBLE, serial=SERIAL, refresh=REFRESH, retry=RETRY, expire=EXPIRE, minimum=MINIMUM,
                        ns0=DNS0, ns1=DNS1)

SOA = SOA.SOA(rdataclass.IN, rdatatype.SOA, DNS0, RESPONSIBLE, SERIAL, REFRESH, RETRY, EXPIRE, MINIMUM)
NS = [NS.NS(rdataclass.IN, rdatatype.NS, DNS0),
      NS.NS(rdataclass.IN, rdatatype.NS, DNS1)]

PARSED_ZONE_FILE = node.Node()
PARSED_ZONE_FILE.rdatasets = [rdataset.from_rdata(TTL, SOA),
                              rdataset.from_rdata_list(TTL, NS)]

VERBOSE = True


class TestDnsBind(unittest.TestCase):
    def test_parse_zone_file(self):
        zone_file = StringIO(ZONE_FILE)
        return_value = dns_bind.parse_zone_file(zone_file, DOMAIN)

        self.assertEqual(return_value, (True, {'parsed_zone_file': PARSED_ZONE_FILE}))

    def test_parse_erroneous_zone_file(self):
        zone_file = StringIO("WRONG")

        with self.assertRaises(exception.SyntaxError):
            dns_bind.parse_zone_file(zone_file, DOMAIN)

    def test_ask_dns_servers(self):
        with patch('shared_dns_bind.gethostbyname') as mock_getbyhostname, \
                patch('dns.message.make_query') as mock_make_query, \
                patch('dns.query.udp') as mock_execute_query:
            return_value = dns_bind.ask_dns_servers(DOMAIN)

        call_count = len(settings.OUR_NAMESERVERS)

        self.assertEqual(mock_getbyhostname.call_count, call_count)
        getbyhostname_calls = [call(nameserver) for nameserver in settings.OUR_NAMESERVERS]
        mock_getbyhostname.assert_has_calls(getbyhostname_calls)

        self.assertEqual(mock_make_query.call_count, call_count)
        make_query_calls = [call(DOMAIN, ANY) for _ in range(call_count)]
        mock_make_query.assert_has_calls(make_query_calls)

        self.assertEqual(mock_execute_query.call_count, call_count)
        execute_query_calls = [call(mock_make_query.return_value, mock_getbyhostname.return_value) for _ in range(call_count)]
        mock_execute_query.assert_has_calls(execute_query_calls)

        self.assertEqual(return_value, (
            True, {'server_responses': {nameserver: mock_execute_query.return_value for nameserver in settings.OUR_NAMESERVERS}}))

    def test_compare_zone_file_and_empty_server_response(self):
        server_responses = {'dns.server': message.Message()}

        with patch_stderr() as stderr, patch_stdout() as stdout:
            return_value = dns_bind.compare_zone_file_and_server_response(PARSED_ZONE_FILE, server_responses, DOMAIN, VERBOSE)

        self.assertIn('doesn\'t return any records', stderr.getvalue())
        self.assertFalse(stdout.getvalue())
        self.assertEqual(return_value, (False, None))

    def test_compare_zone_file_and_correct_server_response(self):
        the_message = message.Message()
        the_message.answer = [rrset.from_rdata(DOMAIN, TTL, SOA),
                              rrset.from_rdata_list(DOMAIN, TTL, NS)]
        server_responses = {'dns.server': the_message}

        with patch_stderr() as stderr, patch_stdout() as stdout:
            return_value = dns_bind.compare_zone_file_and_server_response(PARSED_ZONE_FILE, server_responses, DOMAIN, VERBOSE)

        self.assertFalse(stderr.getvalue())
        self.assertIn('OK', stdout.getvalue())
        self.assertEqual(return_value, (True, None))

    def test_compare_zone_file_and_server_response_missing_ns(self):
        the_message = message.Message()
        the_message.answer = [rrset.from_rdata(DOMAIN, TTL, SOA)]
        server_responses = {'dns.server': the_message}

        with patch_stderr() as stderr, patch_stdout() as stdout:
            return_value = dns_bind.compare_zone_file_and_server_response(PARSED_ZONE_FILE, server_responses, DOMAIN, VERBOSE)

        self.assertIn('doesn\'t return the following', stderr.getvalue())
        self.assertFalse(stdout.getvalue())
        self.assertEqual(return_value, (False, None))

    def test_compare_zone_file_and_server_response_with_excess_txt(self):
        txt = TXT.TXT(rdataclass.IN, rdatatype.TXT, "excess entry")
        the_message = message.Message()
        the_message.answer = [rrset.from_rdata(DOMAIN, TTL, SOA),
                              rrset.from_rdata_list(DOMAIN, TTL, NS),
                              rrset.from_rdata(DOMAIN, TTL, txt)]
        server_responses = {'dns.server': the_message}

        with patch_stderr() as stderr, patch_stdout() as stdout:
            return_value = dns_bind.compare_zone_file_and_server_response(PARSED_ZONE_FILE, server_responses, DOMAIN, VERBOSE)

        self.assertIn('must not return the following', stderr.getvalue())
        self.assertFalse(stdout.getvalue())
        self.assertEqual(return_value, (False, None))
