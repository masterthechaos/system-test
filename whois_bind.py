#!/usr/bin/env python

"""Verifies that all configured zones of the given bind configuration files have have our nameserver and our technical
contact configured"""

from __future__ import print_function

import sys
import time

import pythonwhois
import settings
from shared import print_stderr, ABORT_ITERATION, execute
from shared_bind import parse_bind_configuration


def main():
    description = """Verifies that all configured zones of the given bind configuration files have have our nameserver
                     and our technical contact configured."""
    file_argument_help = "a list of bind configuration files"
    test_steps = [do_whois, nameservers_are_correct, contact_is_correct, registrar_is_correct, sleep_between_steps]
    return execute(description, file_argument_help, parse_bind_configuration, test_steps)


def do_whois(domain, **_):
    """perform a whois lookup for domain and handle common errors"""
    try:
        whois = pythonwhois.get_whois(domain, True)
    except pythonwhois.shared.WhoisException as exception:
        print_stderr('%s: %s. Skipping all test steps.' % (domain, exception))
        return ABORT_ITERATION, None

    if any(u'Please wait a moment and try again.' in s for s in whois.get('raw')):
        print_stderr('%s: The number of requests per client per time interval is restricted. You have exceeded this limit. Please wait a moment and try again.')
        return ABORT_ITERATION, None
    if any(u'We do not have an entry in our database matching your query.' in s for s in whois.get('raw')):
        print_stderr('%s is not registered.' % domain)
        return ABORT_ITERATION, None
    return True, {'whois_registrar': whois.get('registrar'), 'whois_nameservers': whois.get('nameservers'),
                  'whois_contact': whois.get('contacts', {}).get('tech')}


def registrar_is_correct(domain, whois_registrar, verbose, **_):
    """test if the WHOIS registrar and our registrar match"""
    correct_registrar = settings.REGISTRAR[domain]
    if correct_registrar is None:
        if verbose:
            print('%s has no or no parseable registrar in WHOIS but we know this. Skipping test step.' % domain)
        return True, None

    whois_registrar = ''.join(whois_registrar)
    if whois_registrar != correct_registrar:
        print_stderr(u'{domain} has a wrong registrar. The registrar is: {whois_registrar}. The correct registrar is: {correct_registrar}.'
                     .format(domain=domain,
                             whois_registrar=whois_registrar,
                             correct_registrar=correct_registrar))
        return False, None
    elif verbose:
        print('{domain} registrar is {whois_registrar}. OK.'
              .format(domain=domain,
                      whois_registrar=whois_registrar))
    return True, None


def nameservers_are_correct(domain, whois_nameservers, verbose, **_):
    """test if the domain WHOIS entry has only name servers from correct_nameservers"""
    if whois_nameservers is None:
        print_stderr('%s has no or no parseable name servers in WHOIS.' % domain)
        return False, None

    correct_nameservers = settings.OUR_NAMESERVERS
    if len(set(whois_nameservers) ^ correct_nameservers) > 0:
        print_stderr('{domain} uses invalid name servers in WHOIS. Used servers are: {whois_nameservers}. Correct servers are: {correct_nameservers}.'
                     .format(domain=domain,
                             whois_nameservers=', '.join(whois_nameservers),
                             correct_nameservers=', '.join(correct_nameservers)))
        return False, None
    elif verbose:
        print('{domain} uses these name servers in WHOIS: {whois_nameservers}. OK.'
              .format(domain=domain,
                      whois_nameservers=', '.join(whois_nameservers)))
    return True, None


def contact_is_correct(domain, whois_contact, verbose, **_):
    """test if the contact in the WHOIS entry and our contact data match"""
    correct_contact = settings.WHOIS[domain]
    if correct_contact is None:
        if verbose:
            print('%s has no or no parseable tech contact in WHOIS but we know this. Skipping test step.' % domain)
        return True, None
    if whois_contact is None:
        print_stderr('%s has no or no parseable tech contact in WHOIS.' % domain)
        return False, None

    all_tests_ok = True
    for whois_key, whois_value in whois_contact.items():
        if whois_key in correct_contact:
            correct_value = correct_contact[whois_key]
            if correct_value not in whois_value:
                print_stderr(u'{domain} has a wrong tech contact. The {whois_key} must contain: {correct_value}. It currently is: {whois_value}.'
                             .format(domain=domain,
                                     whois_key=whois_key,
                                     whois_value=whois_value.replace('\n', '\\n'),
                                     correct_value=correct_value))
                all_tests_ok = False
            elif verbose:
                print(u'{domain} tech contact {whois_key} is {whois_value}. OK.'
                      .format(domain=domain,
                              whois_key=whois_key,
                              whois_value=whois_value.replace('\n', '\\n')))
    return all_tests_ok, None


def sleep_between_steps(**_):
    time.sleep(20)  # SWITCH doesn't allow to many queries, so slow down.
    return True, None


if __name__ == '__main__':
    sys.exit(main())
