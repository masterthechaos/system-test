import socket


def gethostbyname(hostname):
    """Return the IP address (a string of the form '255.255.255.255') for a host."""
    return socket.gethostbyname(hostname)
