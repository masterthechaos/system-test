$TTL 1d
;			( serial number, slave refresh period, slave retry time, slave expiration time, maximum cache time )
@	IN	SOA	dns0.chaosnet.ch. hostmaster.chaosnet.ch. ( 2016042201 2h 20m 15d 1h )

;; DNS servers ;;
@	IN	NS	dns0.chaosnet.ch.
	IN	NS	dns1.chaosnet.ch.
	IN	NS	dns2.chaosnet.ch.

;; Mails auf Vritz III ;;
@	IN	MX	10 mx.chaosnet.ch.
	IN	TXT	"v=spf1 a mx ptr -all" ; SPF, scharf
mail	IN	A	78.46.66.232
mx	IN	A	78.46.66.232
autoconfig	IN	CNAME	default.chaosnet.ch.
_autodiscover._tcp	IN	SRV	0 0 80 default.chaosnet.ch.

;; Webs auf Vritz III ;;
@	IN	A	78.46.66.232 ; domain ohne www
www	IN	A	78.46.66.232
sftp	IN	A	78.46.66.232
ftp	IN	A	78.46.66.232

;; DNS ;;
dns0	IN	A	78.46.83.226 ; Vritz V (master)
dns1	IN	A	78.46.83.234 ; Vritz IV 
dns2	IN	A	78.46.66.232 ; Vritz III 

;; Andere Server ;;
vritz	IN	A	78.46.83.230 ; VritzNG, vorübergehender Power-Vritz
vritz3	IN	A	78.46.66.232 ; vritz3
vritz3b	IN	A	178.63.107.184 ; vritz3b@vritz.chaosnet.ch
vritz4	IN	A	78.46.83.234 ; Vritz4@r1.chaosnet.ch
vritz5	IN	A	78.46.83.226 ; vritz5@r1.chaosnet.ch
vritz6	IN	A	78.46.83.253 ; vritz6@r1.chaosnet.ch
r1	IN	A	78.46.83.236 ; Master für VMs

;; Subdomains ;;
admin	IN	CNAME	vritz3
maus	IN	CNAME	vritz3
philippe	IN	CNAME	vritz3
christian	IN	CNAME 	vritz3
piwik	IN	CNAME	vritz4
support	IN	CNAME	vritz4
default	IN	CNAME	vritz3 ; Default Server for incorrectly configured domains
