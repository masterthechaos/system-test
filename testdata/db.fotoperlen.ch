$TTL 1h
;			( serial number, slave refresh period, slave retry time, slave expiration time, maximum cache time )
@	IN	SOA	dns0.chaosnet.ch. hostmaster.chaosnet.ch. ( 2014091801 2h 20m 15d 1h )

;; DNS servers ;;
@	IN	NS	dns0.chaosnet.ch.
	IN	NS	dns1.chaosnet.ch.
	IN	NS	dns2.chaosnet.ch.

;; Mails auf Vritz III ;;
@	IN	MX	10 mx.chaosnet.ch.
	IN	TXT	"v=spf1 a mx ptr -all" ; SPF, scharf
mail	IN	A	78.46.66.232

;; Webs sonstwo ;;
@	IN	A	78.46.19.74
www	IN	CNAME	fotoperlen.jimdo.com.
neu	IN	A	78.46.83.234
