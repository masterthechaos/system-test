$TTL 1d
;			( serial number, slave refresh period, slave retry time, slave expiration time, maximum cache time )
@	IN	SOA	dns0.chaosnet.ch. hostmaster.chaosnet.ch. ( 2016032400 2h 20m 15d 1h )

;; DNS servers ;;
@	IN	NS	dns0.chaosnet.ch.
	IN	NS	dns1.chaosnet.ch.
	IN	NS	dns2.chaosnet.ch.

;; Mails auf Vritz III ;;
@	IN	MX	10 mx.chaosnet.ch.
@	IN	TXT	"v=spf1 a mx ptr -all" ; SPF, scharf
mail	IN	A	78.46.66.232

;; Webs auf Vritz IV ;;
@	IN	A	78.46.66.232 ; domain ohne www
www	IN	A	78.46.66.232
sftp	IN	A	78.46.66.232
ftp	IN	A	78.46.66.232
;*	IN	A	78.46.66.232 ; alle subdomains

;; Weiteres ;;
fotos		IN	CNAME 	vritz4.chaosnet.ch.
