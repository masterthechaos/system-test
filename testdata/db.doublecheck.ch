$TTL 1d
;			( serial number, slave refresh period, slave retry time, slave expiration time, maximum cache time )
@	IN	SOA	dns0.chaosnet.ch. hostmaster.chaosnet.ch. ( 2016081200 2h 20m 15d 1h )

;; DNS servers ;;
@	IN	NS	dns0.chaosnet.ch.
	IN	NS	dns1.chaosnet.ch.
	IN	NS	dns2.chaosnet.ch.

;; Mails bei zurichnetgroup ;;
@	IN	MX	10 mx.chng.ch.
mail	IN	CNAME	mail.redirect.chng.ch.
autodiscover	IN	CNAME	autodiscover.redirect.chng.ch.
webmail	IN	CNAME	webmail.redirect.chng.ch.

;; Webs auf Vritz IV ;;
@	IN	A	78.46.83.234 ; domain ohne www
www	IN	A	78.46.83.234
sftp	IN	A	78.46.83.234
ftp	IN	A	78.46.83.234
;*	IN	A	78.46.66.234 ; alle subdomains
