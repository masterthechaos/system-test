from __future__ import print_function

import unittest

import shared
from shared import ABORT_ITERATION, ERROR, OK


def successful_step(executed_steps, **_):
    executed_steps.append(successful_step)
    return OK, None


def erroneous_step(executed_steps, **_):
    executed_steps.append(erroneous_step)
    return ERROR, None


def aborting_step(executed_steps, **_):
    executed_steps.append(aborting_step)
    return ABORT_ITERATION, None


def modify_test_data(executed_steps, **_):
    executed_steps.append(modify_test_data)
    return OK, {'testdata': True}


def check_for_test_data(executed_steps, testdata, **_):
    executed_steps.append(check_for_test_data)
    if testdata:
        return OK, None
    else:
        return ERROR, None


class TestSharedExecuteTestSteps(unittest.TestCase):
    def execute_and_verify(self, steps_to_execute, expected_return_value, expected_executed_steps=None):
        if expected_executed_steps is None:
            expected_executed_steps = steps_to_execute

        executed_steps = []
        return_value = shared.execute_test_steps(steps_to_execute, {'executed_steps': executed_steps})

        self.assertEqual(return_value, expected_return_value)
        self.assertEqual(executed_steps, expected_executed_steps)

    def test_with_empty_parameters(self):
        self.execute_and_verify([], True)

    def test_with_one_successful_step(self):
        self.execute_and_verify([successful_step], True)

    def test_with_one_erroneous_step(self):
        self.execute_and_verify([erroneous_step], False)

    def test_that_return_value_is_false_even_if_subsequent_steps_are_successful(self):
        self.execute_and_verify([erroneous_step, successful_step], False)

    def test_that_subsequent_steps_are_not_executed_if_a_step_returns_abort_iteration(self):
        self.execute_and_verify([aborting_step, successful_step], False, [aborting_step])

    def test_passing_data_between_test_steps(self):
        self.execute_and_verify([modify_test_data, check_for_test_data], True)


if __name__ == '__main__':
    unittest.main()
