Verifies that configured services (HTTP, DNS, Mail) work as configured.

Dependencies: see requirements.text

Execute the tests using 'python -m unittest discover'
