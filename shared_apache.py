import socket
import http.client  # Python2 import httplib
import ssl

import dns.resolver


def try_reverse_lookup(ip_address):
    """try to lookup the IP address and return the hostname. On errors return the given IP address."""
    try:
        return socket.gethostbyaddr(ip_address)[0]
    except:
        return ip_address


def resolve_ip_address(hostname, nameserver):
    """queries 'nameserver' for 'hostname'. Returns the IP address as String"""
    resolver = dns.resolver.Resolver()
    resolver.nameservers = [nameserver]
    answer = resolver.query(hostname)
    return answer.rrset[0].to_text()


def http_request(hostname, ssl_enabled, method='HEAD', url=''):
    """make a HTTP(S) request to 'hostname'. Returns a tuple with the HTTP-status and the HTTP-response."""
    if ssl_enabled:
        connection = httplib.HTTPSConnection(hostname)
    else:
        connection = httplib.HTTPConnection(hostname)
    connection.request(method, url)
    response = connection.getresponse()
    return response.status, response.reason


def ssl_cert(hostname, cert_store):
    """return the SSL certificate of a connection to 'hostname'"""
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ssl_sock = ssl.wrap_socket(sock, cert_reqs=ssl.CERT_REQUIRED, ca_certs=cert_store)
    ssl_sock.connect((hostname, 443))
    cert = ssl_sock.getpeercert()
    ssl_sock.close()
    return cert
