from io import StringIO  # Python2: from io import BytesIO as StringIO

from mock import patch


def patch_stderr():
    return patch('sys.stderr', new=StringIO())


def patch_stdout():
    return patch('sys.stdout', new=StringIO())
