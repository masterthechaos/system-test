#!/usr/bin/env python

"""Verifies that all configured hostnames of the given apache configuration files are hosted on our servers, respond to
 HTTP(S) requests and the certificate isn't expired."""

from __future__ import print_function

import re
import sys
from datetime import datetime

import dateutil.parser
import dns.resolver

from backports.ssl_match_hostname import match_hostname, CertificateError
from shared import print_stderr, ABORT_ITERATION, execute
import shared_apache
import settings


def main():
    description = """Verifies that all configured hostnames of the given apache configuration files are hosted on our
                     servers, respond to HTTP(S) requests and the certificate isn't expired."""
    file_argument_help = 'a list of apache configuration files'
    test_steps = [server_is_ours, server_responds, get_ssl_cert, certificate_expired, certificate_subject_matches_hostname]
    return execute(description, file_argument_help, parse_virtualhost_configuration, test_steps)


def parse_virtualhost_configuration(file):
    """iterates over all ServerName's and ServerAlias'es in 'file'. Returns the hostname and if SSLEngine is on or off for its VirtualHost."""
    in_virtual_host_block = False
    for line in file:
        # Ignore comments
        if re.match(r'^\s*#', line):
            continue
        # Start of VirtualHost block
        if '<VirtualHost' in line:
            in_virtual_host_block = True
            hostnames = []
            ssl_enabled = False
        elif in_virtual_host_block:
            if 'ServerName' in line:
                hostnames.append(line.split()[1])
            elif 'ServerAlias' in line:
                hostnames.extend(line.split()[1:])
            elif 'SSLEngine on' in line:
                ssl_enabled = True
            elif '</VirtualHost>' in line:
                in_virtual_host_block = False
                for hostname in hostnames:
                    yield {'hostname': hostname, 'ssl_enabled': ssl_enabled}


def server_is_ours(hostname, verbose, **_):
    """test if the hostname resolves to one of our servers"""
    try:
        ip_address = shared_apache.resolve_ip_address(hostname, settings.THIRD_PARTY_DNS)
        if ip_address not in settings.OUR_HOST_IPS:
            print_stderr('%s is not hosted on our servers, but on %s.' % (hostname, shared_apache.try_reverse_lookup(ip_address)))
            return ABORT_ITERATION, None
        elif verbose:
            print('%s is hosted on %s. OK.' % (hostname, ip_address))
    except dns.resolver.NXDOMAIN:
        print_stderr('%s not found using DNS server %s.' % (hostname, settings.THIRD_PARTY_DNS))
        return ABORT_ITERATION, None
    except dns.exception.Timeout:
        print_stderr('Timeout resolving %s.' % hostname)
        return ABORT_ITERATION, None
    return True, None


def server_responds(hostname, ssl_enabled, verbose, **_):
    """test if the server responds to HTTP(S) requests."""
    status, reason = shared_apache.http_request(hostname, ssl_enabled)
    if status not in settings.OK_HTTP_CODES:
        print_stderr('Error accessing %s (SSL=%s). Response: %d %s' % (hostname, ssl_enabled, status, reason))
        return ABORT_ITERATION, None
    elif verbose:
        https = 'S' if ssl_enabled else ''
        print('%s replied with %d %s to a HTTP%s request. OK.' % (hostname, status, reason, https))
    return True, None


def get_ssl_cert(hostname, ssl_enabled, **_):
    if not ssl_enabled:
        return True, {'cert': None}
    else:
        return True, {'cert': shared_apache.ssl_cert(hostname, settings.CERT_STORE)}


def certificate_expired(cert, hostname, ssl_enabled, verbose, **_):
    if not ssl_enabled:
        return True, None
    try:
        expire_at = dateutil.parser.parse(cert['notAfter'])
    except ValueError as e:
        print_stderr('%s has unparsable SSL certificate date format: %s' % (hostname, e))
        return False, None
    expire_in = expire_at - datetime.now(tz=dateutil.tz.tzutc()).replace(microsecond=0)
    if expire_in > settings.CERT_EXPIRY_WARN:
        if verbose:
            print('%s SSL certificate expires in %s. OK' % (hostname, expire_in))
        return True, None
    else:
        if expire_in.days < 0:
            print_stderr('%s SSL certificate expired %s ago.' % (hostname, abs(expire_in)))
        else:
            print_stderr('%s SSL certificate expires in %s.' % (hostname, expire_in))
        return False, None


def certificate_subject_matches_hostname(cert, hostname, ssl_enabled, verbose, **_):
    if not ssl_enabled:
        return True, None
    try:
        match_hostname(cert, hostname)
        if verbose:
            print('%s matches the name specified in the SSL certificate. OK.' % hostname)
        return True, None
    except CertificateError:
        print_stderr('%s doesn\'t match any of the names specified in the SSL certificate: %s' % (hostname, [element[1] for element in cert['subjectAltName']]))
        return False, None


if __name__ == '__main__':
    sys.exit(main())
