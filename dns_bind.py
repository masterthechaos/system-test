#!/usr/bin/env python

"""Verifies that all our nameservers respond with the values configured in the zone file"""

from __future__ import print_function

import sys

import dns.query
import dns.zone

import shared_dns_bind
import settings
from shared import print_stderr, execute
from shared_bind import parse_bind_configuration


def main():
    description = "Verifies that all our nameservers respond with the values configured in the zone file."
    file_argument_help = "a list of bind configuration files"
    test_steps = [set_dns_name, parse_zone_file, ask_dns_servers, compare_zone_file_and_server_response]
    return execute(description, file_argument_help, parse_bind_configuration, test_steps)


def set_dns_name(domain, **_):
    return True, {'dns_name': dns.name.from_text(domain)}


def parse_zone_file(zone_file, dns_name, **_):
    zone = dns.zone.from_file(zone_file, dns_name, relativize=False)
    return True, {'parsed_zone_file': zone.find_node(dns_name)}


def ask_dns_servers(dns_name, **_):
    responses = {}
    for nameserver in settings.OUR_NAMESERVERS:
        nameserver_ip = shared_dns_bind.gethostbyname(nameserver)
        query = dns.message.make_query(dns_name, dns.rdatatype.ANY)
        responses[nameserver] = dns.query.udp(query, nameserver_ip)
    return True, {'server_responses': responses}


def compare_zone_file_and_server_response(parsed_zone_file, server_responses, dns_name, verbose, **_):
    """test if all asked name servers respond with the values configured in the zone file"""
    checks_ok = True
    for nameserver, response in server_responses.items():
        if not response.answer:
            print_stderr('{nameserver} doesn\'t return any records for {domain}.'
                         .format(nameserver=nameserver,
                                 domain=dns_name.to_text()[:-1]))
            checks_ok = False
        else:
            answer_rdatasets = [answer.to_rdataset() for answer in response.answer]
            for answer_rdataset in answer_rdatasets:
                if answer_rdataset not in parsed_zone_file.rdatasets:
                    print_stderr('{nameserver} must not return the following record for {domain}: {answer}'
                                 .format(nameserver=nameserver,
                                         domain=dns_name.to_text()[:-1],
                                         answer=answer_rdataset))
                    checks_ok = False
            for zone_file_rdataset in parsed_zone_file.rdatasets:
                if zone_file_rdataset not in answer_rdatasets:
                    print_stderr('{nameserver} doesn\'t return the following record for {domain}: {rdataset}'
                                 .format(nameserver=nameserver,
                                         domain=dns_name.to_text()[:-1],
                                         rdataset=zone_file_rdataset))
                    checks_ok = False

    if checks_ok and verbose:
        print('{domain}: all name servers return correct records. OK.'.format(domain=dns_name))
    return checks_ok, None


if __name__ == '__main__':
    sys.exit(main())
