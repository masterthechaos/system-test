from __future__ import print_function

from datetime import datetime, timedelta
import unittest
from io import StringIO  # Python2: from io import BytesIO as StringIO

import dateutil
import dns.resolver
from mock import patch, ANY

import apache
import settings
from shared import ABORT_ITERATION
from shared_test import patch_stderr, patch_stdout

HOSTNAME = 'www.example.org'
SSL_ENABLED = True
SSL_DISABLED = False
VERBOSE = True


def first_element(iterable):
    """Returns the first element of *iterable*"""
    return next(iter(iterable))


class TestApache(unittest.TestCase):
    def test_parse_empty_virtualhost_configuration(self):
        virtualhost_configuration = apache.parse_virtualhost_configuration(StringIO())
        self.assertCountEqual(virtualhost_configuration, [])

    def test_parse_virtualhost_configuration(self):
        config = StringIO("""
                         <VirtualHost 127.0.0.1>
                           ServerName example.org
                         </VirtualHost>
                         <VirtualHost 127.0.0.2>
                           ServerName example.com
                         </VirtualHost>""")
        virtualhost_configuration = apache.parse_virtualhost_configuration(config)
        self.assertCountEqual(virtualhost_configuration, [{'ssl_enabled': False, 'hostname': 'example.org'},
                                                          {'ssl_enabled': False, 'hostname': 'example.com'}])

    def test_parse_virtualhost_configuration_with_ssl(self):
        config = StringIO("""
                         <VirtualHost 127.0.0.1>
                           ServerName example.org
                           SSLEngine on
                           SSLProtocol all -SSLv2
                         </VirtualHost>""")
        virtualhost_configuration = apache.parse_virtualhost_configuration(config)
        self.assertCountEqual(virtualhost_configuration, [{'ssl_enabled': True, 'hostname': 'example.org'}])

    def test_parse_virtualhost_configuration_with_serveralias(self):
        config = StringIO("""
                         <VirtualHost 127.0.0.1>
                           ServerName example.org
                           ServerAlias example.com
                           ServerAlias subdomain1.example.com subdomain2.example.com
                         </VirtualHost>""")
        virtualhost_configuration = apache.parse_virtualhost_configuration(config)
        self.assertCountEqual(virtualhost_configuration, [{'ssl_enabled': False, 'hostname': 'example.org'},
                                                          {'ssl_enabled': False, 'hostname': 'example.com'},
                                                          {'ssl_enabled': False, 'hostname': 'subdomain1.example.com'},
                                                          {'ssl_enabled': False, 'hostname': 'subdomain2.example.com'}])

    def test_parse_virtualhost_configuration_with_comments(self):
        config = StringIO("""
                         #<VirtualHost 127.0.0.1>
                         #  ServerName example.org
                         #   ServerAlias example.com
                         #   ServerAlias subdomain.example.com
                         #</VirtualHost>""")
        virtualhost_configuration = apache.parse_virtualhost_configuration(config)
        self.assertCountEqual(virtualhost_configuration, [])

    def test_parse_broken_virtualhost_configuration(self):
        config = StringIO("<VirtualHost 127.0.0.1>")
        virtualhost_configuration = apache.parse_virtualhost_configuration(config)
        self.assertCountEqual(virtualhost_configuration, [])

    def test_server_is_ours(self):
        ip_of_hostname = first_element(settings.OUR_HOST_IPS)

        with patch('shared_apache.resolve_ip_address', return_value=ip_of_hostname) as mock_resolve_ip_address, \
                patch_stderr() as stderr, patch_stdout() as stdout:
            return_value = apache.server_is_ours(HOSTNAME, VERBOSE)

        mock_resolve_ip_address.assert_called_once_with(HOSTNAME, settings.THIRD_PARTY_DNS)
        self.assertEqual(return_value, (True, None))
        self.assertFalse(stderr.getvalue())
        self.assertIn('OK', stdout.getvalue())
        self.assertIn(ip_of_hostname, stdout.getvalue())

    def test_server_is_not_ours(self):
        ip_of_hostname = '1.2.3.4'
        self.assertNotIn(ip_of_hostname, settings.OUR_HOST_IPS)

        with patch('shared_apache.resolve_ip_address', return_value=ip_of_hostname) as mock_resolve_ip_address, \
                patch_stderr() as stderr, patch_stdout() as stdout:
            return_value = apache.server_is_ours(HOSTNAME, VERBOSE)

        mock_resolve_ip_address.assert_called_once_with(HOSTNAME, settings.THIRD_PARTY_DNS)
        self.assertEqual(return_value, (ABORT_ITERATION, None))
        self.assertIn('not hosted', stderr.getvalue())
        self.assertIn(ip_of_hostname, stderr.getvalue())
        self.assertFalse(stdout.getvalue())

    def test_server_is_ours_not_found(self):
        with patch('shared_apache.resolve_ip_address', side_effect=dns.resolver.NXDOMAIN) as mock_resolve_ip_address, \
                patch_stderr() as stderr, patch_stdout() as stdout:
            return_value = apache.server_is_ours(HOSTNAME, VERBOSE)

        mock_resolve_ip_address.assert_called_once_with(HOSTNAME, settings.THIRD_PARTY_DNS)
        self.assertEqual(return_value, (ABORT_ITERATION, None))
        self.assertIn('not found', stderr.getvalue())
        self.assertFalse(stdout.getvalue())

    def test_server_is_ours_timeout(self):
        with patch('shared_apache.resolve_ip_address', side_effect=dns.exception.Timeout) as mock_resolve_ip_address, \
                patch_stderr() as stderr, patch_stdout() as stdout:
            return_value = apache.server_is_ours(HOSTNAME, VERBOSE)

        mock_resolve_ip_address.assert_called_once_with(HOSTNAME, settings.THIRD_PARTY_DNS)
        self.assertEqual(return_value, (ABORT_ITERATION, None))
        self.assertIn('Timeout', stderr.getvalue())
        self.assertFalse(stdout.getvalue())

    def test_server_responds(self):
        http_status = first_element(settings.OK_HTTP_CODES)

        with patch('shared_apache.http_request', return_value=(http_status, 'OK')) as mock_http_request, \
                patch_stderr() as stderr, patch_stdout() as stdout:
            return_value = apache.server_responds(HOSTNAME, SSL_ENABLED, VERBOSE)

        mock_http_request.assert_called_once_with(HOSTNAME, SSL_ENABLED)
        self.assertEqual(return_value, (True, None))
        self.assertFalse(stderr.getvalue())
        self.assertIn('OK', stdout.getvalue())
        self.assertIn(str(http_status), stdout.getvalue())

    def test_server_not_responding(self):
        http_status = 666
        self.assertNotIn(http_status, settings.OK_HTTP_CODES)

        with patch('shared_apache.http_request', return_value=(http_status, 'ERROR')) as mock_http_request, \
                patch_stderr() as stderr, patch_stdout() as stdout:
            return_value = apache.server_responds(ANY, SSL_ENABLED, VERBOSE)

        mock_http_request.assert_called_once_with(None, SSL_ENABLED)
        self.assertEqual(return_value, (ABORT_ITERATION, None))
        self.assertIn('Error', stderr.getvalue())
        self.assertIn(str(http_status), stderr.getvalue())
        self.assertFalse(stdout.getvalue())

    def test_certificate_expired_without_ssl(self):
        with patch_stderr() as stderr, patch_stdout() as stdout:
            return_value = apache.certificate_expired(ANY, HOSTNAME, SSL_DISABLED, VERBOSE)

        self.assertEqual(return_value, (True, None))
        self.assertFalse(stderr.getvalue())
        self.assertFalse(stdout.getvalue())

    def test_certificate_not_expired(self):
        not_expired = datetime.now(tz=dateutil.tz.tzutc()) + settings.CERT_EXPIRY_WARN + timedelta(days=1)
        cert = {'notAfter': not_expired.strftime('%b %d %H:%M:%S %Y %Z')}

        with patch_stderr() as stderr, patch_stdout() as stdout:
            return_value = apache.certificate_expired(cert, HOSTNAME, SSL_ENABLED, VERBOSE)

        self.assertEqual(return_value, (True, None))
        self.assertFalse(stderr.getvalue())
        self.assertIn('OK', stdout.getvalue())

    def test_certificate_expires_soon(self):
        expires_soon = datetime.now(tz=dateutil.tz.tzutc()) + settings.CERT_EXPIRY_WARN - timedelta(days=1)
        cert = {'notAfter': expires_soon.strftime('%b %d %H:%M:%S %Y %Z')}

        with patch_stderr() as stderr, patch_stdout() as stdout:
            return_value = apache.certificate_expired(cert, HOSTNAME, SSL_ENABLED, VERBOSE)

        self.assertEqual(return_value, (False, None))
        self.assertIn('expires', stderr.getvalue())
        self.assertFalse(stdout.getvalue())

    def test_certificate_expired(self):
        expired = datetime.now(tz=dateutil.tz.tzutc()) - timedelta(days=1)
        cert = {'notAfter': expired.strftime('%b %d %H:%M:%S %Y %Z')}

        with patch_stderr() as stderr, patch_stdout() as stdout:
            return_value = apache.certificate_expired(cert, HOSTNAME, SSL_ENABLED, VERBOSE)

        self.assertEqual(return_value, (False, None))
        self.assertIn('expired', stderr.getvalue())
        self.assertFalse(stdout.getvalue())

    def test_certificate_expired_unparsable_date(self):
        unparsable_date = 'blubber'
        cert = {'notAfter': unparsable_date}

        with patch_stderr() as stderr, patch_stdout() as stdout:
            return_value = apache.certificate_expired(cert, HOSTNAME, SSL_ENABLED, VERBOSE)

        self.assertEqual(return_value, (False, None))
        self.assertIn('has unparsable SSL certificate date format', stderr.getvalue())
        self.assertFalse(stdout.getvalue())

    def test_certificate_subject_matches_hostname_without_ssl(self):
        with patch_stderr() as stderr, patch_stdout() as stdout:
            return_value = apache.certificate_subject_matches_hostname(ANY, HOSTNAME, SSL_DISABLED, VERBOSE)

        self.assertEqual(return_value, (True, None))
        self.assertFalse(stderr.getvalue())
        self.assertFalse(stdout.getvalue())

    def test_certificate_subject_matches_hostname(self):
        cert = {'subjectAltName': [('DNS', '*.example.org')]}

        with patch_stderr() as stderr, patch_stdout() as stdout:
            return_value = apache.certificate_subject_matches_hostname(cert, HOSTNAME, SSL_ENABLED, VERBOSE)

        self.assertEqual(return_value, (True, None))
        self.assertFalse(stderr.getvalue())
        self.assertIn('OK', stdout.getvalue())

    def test_certificate_subject_not_matching_hostname(self):
        cert = {'subjectAltName': [('DNS', '*.example.com')]}

        with patch_stderr() as stderr, patch_stdout() as stdout:
            return_value = apache.certificate_subject_matches_hostname(cert, HOSTNAME, SSL_ENABLED, VERBOSE)

        self.assertEqual(return_value, (False, None))
        self.assertIn('doesn\'t match', stderr.getvalue())
        self.assertFalse(stdout.getvalue())


if __name__ == '__main__':
    unittest.main()
