from collections import defaultdict
from datetime import timedelta

# IPs of all our Web servers. If a website ist hosted here it is considered ours
OUR_HOST_IPS = frozenset(['78.46.66.232', '78.46.83.234'])
# Names of all our DNS servers
OUR_NAMESERVERS = frozenset(['dns0.chaosnet.ch', 'dns1.chaosnet.ch', 'dns2.chaosnet.ch'])
# HTTP response codes telling that a site is running correctly
OK_HTTP_CODES = frozenset([200, 302, 301])
# To get an external view: A DNS server that replies to queries but is not our own.
THIRD_PARTY_DNS = '8.8.8.8'
# Correct Whois data for reference
WHOIS = defaultdict(
    lambda: {'organization': 'Ch@os Internet Production', 'name': 'Frey', 'street': 'Neustadtstrasse 38', 'city': 'Luzern', 'postalcode': '6003',
             'country': 'Switzerland', 'phone': '325124825', 'email': 'hostmaster@chaosnet.ch'},
    {
        'hundeschule-happylife.ch': {'name': 'Krauer Mathias'},
        'hundeshop-happylife.ch': {'name': 'Krauer Mathias'},
        'maerchentriengen.ch': {'name': 'Samuel Willimann'},
        'nostalgiechoerli.ch': {'name': 'Samuel Willimann'},
        'dwmakeup.ch': {'name': 'Samuel Willimann'},
        'wistone.ch': {'name': 'Samuel Willimann'},
        'patrick-felder.ch': {'name': 'Felder Patrick'},
        'alinebrun.ch': None,  # Switchplus
        'claudia-muff.ch': None,  # Switchplus
    })
# The correct registrar
REGISTRAR = defaultdict(lambda: 'hosttech GmbH',
                        {
                            'dafcoaching.com': '1API GmbH',
                            'dafproductions.com': '1API GmbH',
                            'meabroalamor.com': '1API GmbH',
                            'micagellert.com': '1API GmbH',
                            'mivolution.com': '1API GmbH',
                            'roeoesli.net': '1API GmbH',
                            'soazza.com': '1API GmbH',
                            'vacanzasmaya.com': 'Register.com, Inc.',
                            'dwmakeup.ch': 'Metanet AG',
                            'maerchentriengen.ch': 'Metanet AG',
                            'nostalgiechoerli.ch': 'Metanet AG',
                            'wistone.ch': 'Metanet AG'
                        })
# Warn before the certificate expires
CERT_EXPIRY_WARN = timedelta(days=60)
# The root certificate store
# CERT_STORE = 'testdata/ca-certificates.crt'  # Use this certificate store for testing purposes if the system doesn't have a certificate store
CERT_STORE = '/etc/ssl/certs/ca-certificates.crt'
# Server used to send mail, when testing mail accounts
SMTP_SERVER = 'mail.chaosnet.ch'
# Sender address used, when testing mail accounts
SENDER_ADDRESS = 'production@chaosnet.ch'
