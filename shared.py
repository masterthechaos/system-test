from __future__ import print_function

import sys
import argparse
import os.path
import errno


ABORT_ITERATION = -1
ERROR = 0  # or False
OK = 1  # or True


def execute(description, file_argument_help, file_parser, test_steps):
    """Parses configuration files with *file_parser* and executes all *test_steps* with every iteration of *file_parser*.

    The returned exit status can be:
       0: All tests successful
       1: Some tests failed
       2: No such file or directory (ENOENT)
      22: A test step was called with invalid arguments (EINVAL)

    Uses a dict that is unpacked into every test step and updated with a new dict every test step can return.

    = File Parser =
    A file parser takes a file like object as its only parameter and must implement the iterator protocol. Every iteration must return a dict with data for the
    test steps.

    = Test steps =
    Data is passed between the steps using a dict. The dict is initialised with the results of the *ArgumentParser* and the *file_parser*. Every test step can
    access its data and append more data.
    The dict is unpacked when calling a test step function. Define the last parameter to catch all unknown entries of the dict using **kwargs.

    A test step must return a tuple with two values:
     1. The return code of the test:
        -1 (*ABORT_ITERATION*): This test step war erroneous. Abort this test iteration and continue with the next. The exit status of the program will be 1.
         0 (*ERROR* or *False*): This test step was erroneous. Still executes the following test steps that may result in even more errors. The exit status of
                                 the program will be 1.
         1 (*OK* or *True*): This test step was successful. If all test steps where successful, the exit status of the program will be 0.
     2. An dict with more data to be used in the following test steps or *None*. The dict will be merged into the test data dict.
    """
    args = parse_arguments(description, file_argument_help)
    if not all_files_exist(args.files):
        return errno.ENOENT
    all_tests_ok = True
    for filename in args.files:
        with open(filename, 'r') as file:
            for test_data in file_parser(file):
                test_data.update(vars(args))
                all_tests_ok = execute_test_steps(test_steps, test_data) and all_tests_ok
    if not all_tests_ok:
        return 1


def execute_test_steps(test_steps, test_data):
    """Executes all *test_steps*. Returns *False* if a step failed."""
    all_steps_ok = True
    for test_step in test_steps:
        (return_code, more_test_data) = test_step(**test_data)
        if return_code in (ERROR, ABORT_ITERATION):
            all_steps_ok = False
        if return_code == ABORT_ITERATION:
            break
        test_data.update(more_test_data or {})
    return all_steps_ok


def parse_arguments(description, file_argument_help):
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-v', '--verbose', help='increase output verbosity', action='store_true')
    parser.add_argument('files', type=str, nargs='+', help=file_argument_help)
    return parser.parse_args()


def all_files_exist(filenames):
    """Check if all *filenames* exist. Prints a message to STDERR and returns *False* if a file doesn't exist."""
    for filename in filenames:
        if not os.path.isfile(filename):
            print_stderr('Unknown file "%s"' % filename)
            return False
    return True


def print_stderr(*objs):
    print(*objs, file=sys.stderr)
