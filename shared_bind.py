import re


def parse_bind_configuration(file):
    """iterates over all zones in 'file'. Returns the domain and the file name of the zone file."""
    for line in file:
        # Ignore comments
        if re.match(r'^\s*[#/]', line):
            continue

        if 'zone "' in line:
            domain = line.split('"', 2)[1]
        elif 'file "' in line:
            zone_file = line.split('"', 2)[1]
            yield {'domain': domain, 'zone_file': zone_file}
